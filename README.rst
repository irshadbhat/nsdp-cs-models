Pretrained models for Neural Stacking Dependency Parsers
========================================================

----

Neural Stacking Dependency Parser models for monolingual, multilingual and code switching data.
